<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php get_sidebar( 'main' ); ?>

			<div class="site-info">
				<?php do_action( 'twentythirteen_credits' ); ?>
				<?php /*?><a href="<?php echo esc_url( __( 'http://wordpress.org/', 'twentythirteen' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'twentythirteen' ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentythirteen' ), 'WordPress' ); ?></a><?php */?>
                <ul class="social-buttons">
          <li><a href="https://www.facebook.com/pages/Voice-Verso/1540202696240165" title="Facebook"><i class="genericon genericon-facebook-alt"></i></a> </li>
           <li><a href="https://twitter.com/VoiceVerso" title="Twitter"><i class="genericon genericon-twitter"></i></a> </li>
           <li><a href="https://plus.google.com/103960191639723691174" title ="Google+"><i class="genericon genericon-googleplus-alt"></i></a></li>
          <li><a href="https://www.linkedin.com/company/voice-verso" title="Linke-din"><i class="genericon genericon-linkedin"></i></a> </li>
           </ul>
           <ul class="quicklinks">
                        <li><a href="privacy.php">Privacy Policy</a></li>
                        <li><a href="terms.php">Terms of Use</a></li>
           </ul>
           <p>Copyright &copy; VoiceVerso 2015</p>     
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>