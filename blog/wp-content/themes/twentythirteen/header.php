<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title>VoiceVerso-Blog!</title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
 	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
   
</head>

<body <?php body_class(); ?>>

	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
            <div class="home-link" >
            <div class="leftsection">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" style="display: inline-block;">
				<h1 class="site-title"><img src="<?php bloginfo('template_directory'); ?>/images/blog_logo.png"/></h1>
			</a>
            <h2 class="site-description">Start writing your stories, poems, articles etc.,with Voice Verso</h2>
            </div>
            
            <div class="rightsection">
               <a title="Ink It!" class="r-btn" style="display: inline-block;" target="_new" href="http://voiceverso.com">Start Writing!</a>
            <ul class="social-buttons">
          <li><a href="https://www.facebook.com/pages/Voice-Verso/1540202696240165" title="Facebook"><i class="genericon genericon-facebook-alt"></i></a> </li>
           <li><a href="https://twitter.com/VoiceVerso" title="Twitter"><i class="genericon genericon-twitter"></i></a> </li>
           <li><a href="https://plus.google.com/103960191639723691174" rel="publisher" class="page-scroll" data-toggle="tooltip" data-placement="bottom" title="Google+"><i class="genericon genericon-googleplus-alt"></i></a> </li>
          <li><a href="https://www.linkedin.com/company/voice-verso" title="Linke-din"><i class="genericon genericon-linkedin"></i></a> </li>
           </ul> 
            </div>
            
            </div>
			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<button class="menu-toggle"><?php _e( 'Menu', 'twentythirteen' ); ?></button>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
					<?php /*?><?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?><?php */?>
					<?php get_search_form(); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->
		</header><!-- #masthead -->
           
		<div id="main" class="site-main">

   