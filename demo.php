<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<!--        <img class="lazy" data-src="images/banner-1.jpg" />
        <img class="lazy" data-src="images/banner-2.jpg" />
        <img class="lazy" data-src="images/banner-3.jpg" />
        <img class="lazy" data-src="images/process-image.jpg" />
        <img class="lazy" data-src="images/A.jpg" />
        <img class="lazy" data-src="images/d.jpg" />
        <img class="lazy" data-src="images/e.jpg" />
        <img class="lazy" data-src="images/f.jpg" />
        <img class="lazy" data-src="images/g.jpg" />
        <img class="lazy" data-src="images/c.jpg" />-->


        <div class="scroll">This is Div 1</div>
        <div class="scroll">This is Div 2</div>
        <div class="scroll">This is Div 3</div>
        <div class="scroll">This is Div 4</div>
        <div class="scroll">This is Div 5</div>
        <div class="scroll">This is Div 6</div>
        <div class="scroll">This is Div 7</div>
        <div class="scroll">This is Div 8</div>
        <div class="scroll">This is Div 9</div>
        <div class="scroll">This is Div 10</div>

    </body>
    <script src="https://code.jquery.com/jquery-2.2.2.min.js" ></script>
    <script src="js/jquery.jscroll.min.js"></script>
    <script>


        $(function () {
           $('.scroll').jscroll();
           
           $('.infinite-scroll').jscroll({
    loadingHtml: '<img src="loading.gif" alt="Loading" /> Loading...',
    padding: 20,
    nextSelector: 'a.jscroll-next:last',
    contentSelector: 'li'
});
        });

    </script>
    <style>
        div.scroll {
            width: 700px; 
            height: 467px; 
            display: block;
            /*background-color: blue;*/
            border: 1px solid black;
        }


    </style>
</html>
