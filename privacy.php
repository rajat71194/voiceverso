<?php 
ob_start();

//database connection included!
require('includes/connect.php');
include('constents.php');
 // database checking for singup process
//If the values are posted insert them into the database
?>
<!DOCTYPE html>

<html lang="en">

<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="description" content="">

<meta name="author" content="">

<title>Privacy Policy-VoiceVerso</title>



<!-- Bootstrap Core CSS -->

<link href="<?php echo CSS; ?>bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->

<link href="<?php echo CSS; ?>agency.css" rel="stylesheet">

<link href="<?php echo CSS; ?>shoutstory.css" rel="stylesheet">



<!-- Custom Fonts -->

<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

<link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>

<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>



<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<!--[if lt IE 9]>

        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->

<script src="<?php echo JS;?>jquery.js"></script> 


<script type="text/javascript" >
   $(document).ready(function() {
       $( "#signupbtn" ).click(function(e) {
          e.preventDefault();
          var person_id = $("#personid").val();
          var email = $("#singup-email").val();
          var password = $("#singup-password").val();
          if(email==0 && password ==0){
               $("#singup-email").css("border","1px solid red");
               $("#singup-password").css("border","1px solid red");
          }else{
        $.ajax({
             type: "POST",
             dataType: 'html',
             url: "ajaxsignup.php",
             data: {password:password,email:email}
             }).done(function(msg) {
             if(msg==1){
               $("#already").show();
              }else{
                 $("#confirm").show();		
                    return false;
              }
        });

	 }
   });
     $( "#loginbtn" ).click(function(e) {
          e.preventDefault();
          var email = $("#email").val();
          var password = $("#password").val();
          if(email==0 && password ==0){
                 $("#email").css("border","1px solid red");
				 $("#password").css("border"," 1px solid red");
            }else{
  $.ajax({
           type: "POST",
          dataType: 'html',
          url: "ajaxlogin.php",
          data: {password:password,email:email}
          }).done(function(msg) {
          if(msg==1){
              $("#credentials").show();
          }else{
             window.location.href = '<?php echo SITE_URL; ?>/read.php' ;   
             return false;
          }
      });
     }
	  }); 		
  	

	 });


</script> 





</head>



<body id="page-top" class="index privacy-cont">

<nav class="navbar navbar-default" id="inner-content">

  <div class="container"> 

    <!-- Brand and toggle get grouped for better mobile display -->

    <div class="navbar-header page-scroll">

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

      <a class="navbar-brand page-scroll" href="<?php echo SITE_URL;?>"><img src="<?php echo IMAGES;?>voice-verso.png" alt="voice verso"/></a> </div>

    

    <!-- Collect the nav links, forms, and other content for toggling -->

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav navbar-right" id="shoutlogin">

        <li class="hidden"> <a href="#page-top"></a> </li>

        <li><a href="#" class="btn shout-btn btn-primary fl shoutbutton" title="Start Writing!" data-action="sign-in-prompt" data-popover-type ="sign-in" data-popover="Shout your story loud!" data-toggle="modal" data-target="#myModal" style="display:none;">Start Writing!</a></li>

        <li> <a class="page-scroll" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="https://www.facebook.com/pages/Voice-Verso/1540202696240165"><i class="fa fa-facebook"></i></a> </li>

        <li> <a class="page-scroll" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="https://twitter.com/VoiceVerso"><i class="fa fa-twitter"></i></a> </li>

        <li> <a class="page-scroll" data-toggle="tooltip" data-placement="bottom" title="LinkedIn" href="https://www.linkedin.com/company/voice-verso"><i class="fa fa-linkedin"></i></a> </li>

        <!--<li class="last-li"><a class="page-scroll" href="#">Login</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a class="page-scroll" href="#">Signin</a></li>-->

        

      </ul>

    </div>

    <!-- /.navbar-collapse --> 

  </div>

  <!-- /.container-fluid --> 

</nav>





<div class ="container">

  <p class="top-fix">&nbsp;</p>

<div class="col-md-12 inner-content">

      <h3>Information Collection and Use</h3>

<p>When you register and use the web site of VoiceVerso, we collect information, including your:

<ul>

<li>Email address.</li>

<li>Subscription history or other purchases.</li>

<li>Various personalization information.</li>

</ul>

<p>We require the above information to customize the content and the advertising you see, fulfil your requests for products and services, improve our services, contact you, conduct research, and provide anonymous reporting.

Additionally, the information such as your IP address, cookie information, and what pages you request, is automatically saved. This is used for generating statistics about how many people are using the web site, etc.</p>

<h3>Your email address</h3>

<p>Is a required part of your registration. We may use your email address to contact you infrequently or in case of emergencies. In no case shall we ever sell it or give it to any third-party companies.</p>

<h3>Sharing with other members</h3>

<p>As a part of your user profile, we provide you with the ability to disclose an email address, instant messenger user name, and other communication methods.Any logged in member, legitimate or otherwise, has the ability to see this. It is not our responsibility if they misuse such information. In no case shall we ever sell this information to third-party companies.</p>

<h3>Advertising</h3>

<p>Based on the statistical data of the personal information provided by all the members, we may choose to display targeted advertisements. Advertisers may assume that people who interact with, view, or click on targeted ads meet the target criteria - for example, women ages 30-40 from a particular geographic area. Most of our ads are not targeted.

We do not provide any of the personal information to the advertiser when you interact with or view a targeted ad. However, by interacting with or viewing an ad you are consenting to the possibility that the advertiser will make the assumption that you meet the targeting criteria used to display the ad.

</p>

<h3>Cookies</h3>

<p>Many people are unsure what cookies are and are unfamiliar with them.We use cookies to keep you logged in from page to page, so that you do not have to type in your login information on every page of the site.All that the cookie stores is that your username is 'blah', and an encrypted copy of your password.Third-party ads may use cookies to track the types of sites you visit, and show ads accordingly (e.g. poetry ads, or a sports ads if you often visit ESPN).</p>

<h3>Confidentiality and Security</h3>

<p>We limit access to the personal information about you to employees or volunteers who we believe reasonably need to come into contact with that information to provide products or services to you or in order to do their jobs. This is as few people as reasonably possible, and such activity is logged for verification by the others.

Such information includes instant messages sent between users of the site. Only our authorized employees can view these, and only for a pre-specified and valid reason. Viewing this information otherwise, is strictly logged and reviewed.

We reserve the right at all times to disclose any information as we deem necessary to satisfy any applicable law, regulation, legal process or governmental request.Always use caution when giving out any personally identifiable information about yourself or your children in any online communication.

</p>

<h3>We're concerned about your privacy</h3>

<p>If you have a question about your privacy, please contact us and we'll try to clarify it for you!</p>

</div>

</div>

<footer class="clr inner-footer">

    <div class="row">

      <div class="col-md-4"></div>

  

      <div class="col-md-4 copyright_cont">

      <ul class="list-inline social-buttons">

         

          <li><a href="https://www.facebook.com/pages/Voice-Verso/1540202696240165"><i class="fa fa-facebook"></i></a> </li>

           <li><a href="https://twitter.com/VoiceVerso"><i class="fa fa-twitter"></i></a> </li>

          <li><a href="https://www.linkedin.com/company/voice-verso"><i class="fa fa-linkedin"></i></a> </li>

        </ul>

                    <ul class="list-inline quicklinks">

                        <li><a href="privacy.php">Privacy Policy</a>

                        </li>

                        <li><a href="terms.php">Terms of Use</a>

                        </li>

                    </ul>

               <span class="copyright">Copyright &copy; VoiceVerso 2015</span>       

                </div>

                <div class="col-md-4 pull-right"></div>    

    </div>

</footer>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content text-center">
        <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 class="modal-title" id="myModalLabel">Wecome to Voice Verso!</h3>
         </div>
     <div class="modal-body">
         <div class ="row">
              <div id = "credentials" style = "display: none; color: red;">
                Invalid Credentials, Try Again!
              </div>
                 <div class= "form1">
                   <form class="form-horizontal" role="form" method ="POST"  id ="loginForm">
                    <!-- Login form in the modal --> 
                    <span style="color:red"></span>
                      <h2>Login</h2>
                        <div class="form-group">
                            <label class="col-sm-1 col-sm-offset-3 control-label"><span class="glyphicon glyphicon-envelope" title ="Email"></span></label>
                          <div class="col-sm-5">
                              <input id="email" type="text" name ="email" title="Email goes here!" placeholder="Shouter's Email" class="form-control" required/>
                          </div>
                      </div>
                      <div class="form-group"> 
                       <!--Password Login -->
                             <label class="col-sm-1 col-sm-offset-3 control-label"><span class="glyphicon glyphicon-lock" title ="Password"></span></label>
                         <div class="col-sm-5">
                              <input id="password" type="password" title="Password goes here!" placeholder="Shouter's Password" name = "password" class="form-control" maxlength ="15" required/>
                         </div>
                          
                    </div>
                    <!--<div class="form-group">
                     <div class = "col-sm-5">
                       <a type ="link" href ="" class = "link" data-toggle="modal" data-target="#iModal" id = "forgot">Forgot Password?</a>
                     </div>
                     </div>-->
                    
                                  
                       <input type="submit" class= "btn btn-primary btn-lg btn_login" id ="loginbtn" value="Login!" name="login" title ="Log in to shout!"/>
                   </form>
            </div>
                        <p class ="or_sep">----<b>OR</b>----</p>
        <div class ="form2">
                <form class="form-horizontal" role="form" method ="POST" id="userForm">
               <!-- Sign Up Form -->
                      <div id = "already" style = "display: none; color: red;">
                           Email already exists! Try again!
                      </div>
                      <div id = "confirm" style = "display: none; color: green;">
                         Thank you for Signing In. An Email has been sent to your registered Email Address. Please confirm and proceed!
                     </div>
                    <h2> SignUp </h2>
                    <div class="form-group"> 
                    <!-- Sign Up Email -->
                       <label class="col-sm-1 col-sm-offset-3 control-label"><span class="glyphicon glyphicon-envelope" title ="Email"></span></label>
                       <div class="col-sm-5">
                          <input type="text" name ="email" title="Email goes here!" placeholder="Shouter's Email"  id="singup-email" class="form-control" required/>
                          <input type="hidden" id ="personid" value="<?php echo $user_id;?>">
                       </div>
                   </div>
                 <!-- Sign Up Password -->
               <div class="form-group">
                  <label class="col-sm-1 col-sm-offset-3 control-label"><span class="glyphicon glyphicon-lock" title ="Password"></span></label>
                  <div class="col-sm-5">
                     <input type="password" name= "password" title="Password goes here!" placeholder="Shouter's Password"  id ="singup-password" class="form-control" maxlength="15" required/>
                 </div>
             </div>
            
                   <input type="submit" name="signup" class= "btn btn-success btn-lg btn_login" id ="signupbtn" value="Register" title="Sign up to shout!"/>
            </form>
         </div>
      </div>
    </div>
   </div>
 </div>
</div>
    <div class="modal fade" id="iModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	     <div class="modal-dialog">

		   <div class="modal-content">

	        <div class = "modal-header">

	    	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

		      <h3 class="modal-title" id="myModalLabel">Enter your registered Email address here-</h3>

			</div>

	       <div class="modal-body">

		   

		     

	   <div class = "row">
         <div class="col-md-12">
         <div id = "forPassword" style = "display: none;">
		     <p style="color: #69F">An email has been sent to your registered email address. Follow along</p>
            </div>
		   <form method = "post" class = "form-horizontal" action=""> 

		       <div class="form-group"> 
                    <!-- Sign Up Email -->
                       <label class="col-sm-1 control-label"><span class="glyphicon glyphicon-envelope" title ="Email"></span></label>
                       <div class="col-sm-5">
                          <input type="text" name ="email" title="Email goes here!" placeholder="Shouter's Email"  id="pass-email" class="form-control" required/>
                         
                       </div>
                   </div>

			  <input type = "submit" class ="btn btn-success" name ="done" value = "Done" id= "done"> 

			   

		   </form>			 

		</div>	  

		  </div>

		  

		  </div>

		</div>

		</div>

		</div>

	  	

	<!--<p style ="float: right; margin-top: 100px; margin-right: 100px;"><a class="btn btn-link text-center" id="story" title ="Time to shout your stories now!" href ="shout.php">Now that you have an awesome pic! Start shouting your stories!-->

   

 </div>



<!-- jQuery --> 



<!-- Bootstrap Core JavaScript --> 

<script src="js/bootstrap.min.js"></script> 



<!-- Plugin JavaScript --> 

<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<script src="js/classie.js"></script> 

<script src="js/cbpAnimatedHeader.js"></script> 

<script type="text/javascript">

	$(function () {

	  $('[data-toggle="tooltip"]').tooltip()

	})

</script> 



<!-- Custom Theme JavaScript -->



</body>

</html>

