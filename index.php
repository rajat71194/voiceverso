<?php
//ob_start();
session_start();
ini_set('session.use_only_cookies', 1);
+ ini_set('session.cookie_httponly', 1);

if (($_SESSION['id'] != "")) {
    header('Location:read.php');
}

//database connection included!
require('includes/connect.php');
include('constents.php');
$result = mysql_query("SELECT users.profile, users.person_id,users.pename,users.bio,title,
 content,post_id,datetime from blogpost LEFT JOIN `users` 
 ON blogpost.person_id = users.person_id WHERE isPublish ='1' ORDER BY RAND() LIMIT 5") or die(mysql_error());
// database checking for singup process
//If the values are posted insert them into the database
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="p:domain_verify" content="91c4e21de11af31d947bd19d04c1eeb0"/>
        <meta name="author" content="">
        <meta name="alexaVerifyID" content="JtUTkMivxiO4NfQqxvVSSRCYfoQ"/> 


        <!-- for Facebook -->
        <meta property="og:title" content="Voice Verso">
        <meta property="fb:app_id" content="1593320104239178" />
        <meta property="og:type" content="website">
        <meta property="og:image" content="http://www.voiceverso.com/images/logo.jpg">
        <meta property="og:url" content="http://www.voiceverso.com">
        <meta property="og:description" content="Transforming Bloggers into published authors. Voice Verso is a one stop solution, helping writers to publish their content and share them online.">

        <!-- for Twitter -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@VoiceVerso">
        <meta name="twitter:title" content="Voice Verso">
        <meta name="twitter:description" content="Transforming Bloggers into published authors. Use Voice Verso to create the antologies out of your blogging abilites">
        <meta name="twitter:image:src" content="http://www.voiceverso.com/images/logo.jpg"> 

        <script type="text/javascript">
            window.NREUM || (NREUM = {}), __nr_require = function (t, e, n) {
                function r(n) {
                    if (!e[n]) {
                        var o = e[n] = {exports: {}};
                        t[n][0].call(o.exports, function (e) {
                            var o = t[n][1][e];
                            return r(o ? o : e)
                        }, o, o.exports)
                    }
                    return e[n].exports
                }
                if ("function" == typeof __nr_require)
                    return __nr_require;
                for (var o = 0; o < n.length; o++)
                    r(n[o]);
                return r
            }({QJf3ax: [function (t, e) {
                        function n(t) {
                            function e(e, n, a) {
                                t && t(e, n, a), a || (a = {});
                                for (var c = s(e), f = c.length, u = i(a, o, r), d = 0; f > d; d++)
                                    c[d].apply(u, n);
                                return u
                            }
                            function a(t, e) {
                                f[t] = s(t).concat(e)
                            }
                            function s(t) {
                                return f[t] || []
                            }
                            function c() {
                                return n(e)
                            }
                            var f = {};
                            return{on: a, emit: e, create: c, listeners: s, _events: f}
                        }
                        function r() {
                            return{}
                        }
                        var o = "nr@context", i = t("gos");
                        e.exports = n()
                    }, {gos: "7eSDFh"}], ee: [function (t, e) {
                        e.exports = t("QJf3ax")
                    }, {}], 3: [function (t) {
                        function e(t) {
                            try {
                                i.console && console.log(t)
                            } catch (e) {
                            }
                        }
                        var n, r = t("ee"), o = t(1), i = {};
                        try {
                            n = localStorage.getItem("__nr_flags").split(","), console && "function" == typeof console.log && (i.console = !0, -1 !== n.indexOf("dev") && (i.dev = !0), -1 !== n.indexOf("nr_dev") && (i.nrDev = !0))
                        } catch (a) {
                        }
                        i.nrDev && r.on("internal-error", function (t) {
                            e(t.stack)
                        }), i.dev && r.on("fn-err", function (t, n, r) {
                            e(r.stack)
                        }), i.dev && (e("NR AGENT IN DEVELOPMENT MODE"), e("flags: " + o(i, function (t) {
                            return t
                        }).join(", ")))
                    }, {1: 22, ee: "QJf3ax"}], 4: [function (t) {
                        function e(t, e, n, i, s) {
                            try {
                                c ? c -= 1 : r("err", [s || new UncaughtException(t, e, n)])
                            } catch (f) {
                                try {
                                    r("ierr", [f, (new Date).getTime(), !0])
                                } catch (u) {
                                }
                            }
                            return"function" == typeof a ? a.apply(this, o(arguments)) : !1
                        }
                        function UncaughtException(t, e, n) {
                            this.message = t || "Uncaught error with no additional information", this.sourceURL = e, this.line = n
                        }
                        function n(t) {
                            r("err", [t, (new Date).getTime()])
                        }
                        var r = t("handle"), o = t(6), i = t("ee"), a = window.onerror, s = !1, c = 0;
                        t("loader").features.err = !0, t(5), window.onerror = e;
                        try {
                            throw new Error
                        } catch (f) {
                            "stack"in f && (t(1), t(2), "addEventListener"in window && t(3), window.XMLHttpRequest && XMLHttpRequest.prototype && XMLHttpRequest.prototype.addEventListener && window.XMLHttpRequest && XMLHttpRequest.prototype && XMLHttpRequest.prototype.addEventListener && !/CriOS/.test(navigator.userAgent) && t(4), s = !0)
                        }
                        i.on("fn-start", function () {
                            s && (c += 1)
                        }), i.on("fn-err", function (t, e, r) {
                            s && (this.thrown = !0, n(r))
                        }), i.on("fn-end", function () {
                            s && !this.thrown && c > 0 && (c -= 1)
                        }), i.on("internal-error", function (t) {
                            r("ierr", [t, (new Date).getTime(), !0])
                        })
                    }, {1: 9, 2: 8, 3: 6, 4: 10, 5: 3, 6: 23, ee: "QJf3ax", handle: "D5DuLP", loader: "G9z0Bl"}], 5: [function (t) {
                        function e() {
                        }
                        if (window.performance && window.performance.timing && window.performance.getEntriesByType) {
                            var n = t("ee"), r = t("handle"), o = t(1), i = t(2);
                            t("loader").features.stn = !0, t(3), n.on("fn-start", function (t) {
                                var e = t[0];
                                e instanceof Event && (this.bstStart = Date.now())
                            }), n.on("fn-end", function (t, e) {
                                var n = t[0];
                                n instanceof Event && r("bst", [n, e, this.bstStart, Date.now()])
                            }), o.on("fn-start", function (t, e, n) {
                                this.bstStart = Date.now(), this.bstType = n
                            }), o.on("fn-end", function (t, e) {
                                r("bstTimer", [e, this.bstStart, Date.now(), this.bstType])
                            }), i.on("fn-start", function () {
                                this.bstStart = Date.now()
                            }), i.on("fn-end", function (t, e) {
                                r("bstTimer", [e, this.bstStart, Date.now(), "requestAnimationFrame"])
                            }), n.on("pushState-start", function () {
                                this.time = Date.now(), this.startPath = location.pathname + location.hash
                            }), n.on("pushState-end", function () {
                                r("bstHist", [location.pathname + location.hash, this.startPath, this.time])
                            }), "addEventListener"in window.performance && (window.performance.addEventListener("webkitresourcetimingbufferfull", function () {
                                r("bstResource", [window.performance.getEntriesByType("resource")]), window.performance.webkitClearResourceTimings()
                            }, !1), window.performance.addEventListener("resourcetimingbufferfull", function () {
                                r("bstResource", [window.performance.getEntriesByType("resource")]), window.performance.clearResourceTimings()
                            }, !1)), document.addEventListener("scroll", e, !1), document.addEventListener("keypress", e, !1), document.addEventListener("click", e, !1)
                        }
                    }, {1: 9, 2: 8, 3: 7, ee: "QJf3ax", handle: "D5DuLP", loader: "G9z0Bl"}], 6: [function (t, e) {
                        function n(t) {
                            i.inPlace(t, ["addEventListener", "removeEventListener"], "-", r)
                        }
                        function r(t) {
                            return t[1]
                        }
                        var o = (t(1), t("ee").create()), i = t(2)(o), a = t("gos");
                        if (e.exports = o, n(window), "getPrototypeOf"in Object) {
                            for (var s = document; s && !s.hasOwnProperty("addEventListener"); )
                                s = Object.getPrototypeOf(s);
                            s && n(s);
                            for (var c = XMLHttpRequest.prototype; c && !c.hasOwnProperty("addEventListener"); )
                                c = Object.getPrototypeOf(c);
                            c && n(c)
                        } else
                            XMLHttpRequest.prototype.hasOwnProperty("addEventListener") && n(XMLHttpRequest.prototype);
                        o.on("addEventListener-start", function (t) {
                            if (t[1]) {
                                var e = t[1];
                                "function" == typeof e ? this.wrapped = t[1] = a(e, "nr@wrapped", function () {
                                    return i(e, "fn-", null, e.name || "anonymous")
                                }) : "function" == typeof e.handleEvent && i.inPlace(e, ["handleEvent"], "fn-")
                            }
                        }), o.on("removeEventListener-start", function (t) {
                            var e = this.wrapped;
                            e && (t[1] = e)
                        })
                    }, {1: 23, 2: 24, ee: "QJf3ax", gos: "7eSDFh"}], 7: [function (t, e) {
                        var n = (t(2), t("ee").create()), r = t(1)(n);
                        e.exports = n, r.inPlace(window.history, ["pushState"], "-")
                    }, {1: 24, 2: 23, ee: "QJf3ax"}], 8: [function (t, e) {
                        var n = (t(2), t("ee").create()), r = t(1)(n);
                        e.exports = n, r.inPlace(window, ["requestAnimationFrame", "mozRequestAnimationFrame", "webkitRequestAnimationFrame", "msRequestAnimationFrame"], "raf-"), n.on("raf-start", function (t) {
                            t[0] = r(t[0], "fn-")
                        })
                    }, {1: 24, 2: 23, ee: "QJf3ax"}], 9: [function (t, e) {
                        function n(t, e, n) {
                            t[0] = o(t[0], "fn-", null, n)
                        }
                        var r = (t(2), t("ee").create()), o = t(1)(r);
                        e.exports = r, o.inPlace(window, ["setTimeout", "setInterval", "setImmediate"], "setTimer-"), r.on("setTimer-start", n)
                    }, {1: 24, 2: 23, ee: "QJf3ax"}], 10: [function (t, e) {
                        function n() {
                            f.inPlace(this, p, "fn-")
                        }
                        function r(t, e) {
                            f.inPlace(e, ["onreadystatechange"], "fn-")
                        }
                        function o(t, e) {
                            return e
                        }
                        function i(t, e) {
                            for (var n in t)
                                e[n] = t[n];
                            return e
                        }
                        var a = t("ee").create(), s = t(1), c = t(2), f = c(a), u = c(s), d = window.XMLHttpRequest, p = ["onload", "onerror", "onabort", "onloadstart", "onloadend", "onprogress", "ontimeout"];
                        e.exports = a, window.XMLHttpRequest = function (t) {
                            var e = new d(t);
                            try {
                                a.emit("new-xhr", [], e), u.inPlace(e, ["addEventListener", "removeEventListener"], "-", o), e.addEventListener("readystatechange", n, !1)
                            } catch (r) {
                                try {
                                    a.emit("internal-error", [r])
                                } catch (i) {
                                }
                            }
                            return e
                        }, i(d, XMLHttpRequest), XMLHttpRequest.prototype = d.prototype, f.inPlace(XMLHttpRequest.prototype, ["open", "send"], "-xhr-", o), a.on("send-xhr-start", r), a.on("open-xhr-start", r)
                    }, {1: 6, 2: 24, ee: "QJf3ax"}], 11: [function (t) {
                        function e(t) {
                            var e = this.params, r = this.metrics;
                            if (!this.ended) {
                                this.ended = !0;
                                for (var i = 0; c > i; i++)
                                    t.removeEventListener(s[i], this.listener, !1);
                                if (!e.aborted) {
                                    if (r.duration = (new Date).getTime() - this.startTime, 4 === t.readyState) {
                                        e.status = t.status;
                                        var a = t.responseType, f = "arraybuffer" === a || "blob" === a || "json" === a ? t.response : t.responseText, u = n(f);
                                        if (u && (r.rxSize = u), this.sameOrigin) {
                                            var d = t.getResponseHeader("X-NewRelic-App-Data");
                                            d && (e.cat = d.split(", ").pop())
                                        }
                                    } else
                                        e.status = 0;
                                    r.cbTime = this.cbTime, o("xhr", [e, r, this.startTime])
                                }
                            }
                        }
                        function n(t) {
                            if ("string" == typeof t && t.length)
                                return t.length;
                            if ("object" != typeof t)
                                return void 0;
                            if ("undefined" != typeof ArrayBuffer && t instanceof ArrayBuffer && t.byteLength)
                                return t.byteLength;
                            if ("undefined" != typeof Blob && t instanceof Blob && t.size)
                                return t.size;
                            if ("undefined" != typeof FormData && t instanceof FormData)
                                return void 0;
                            try {
                                return JSON.stringify(t).length
                            } catch (e) {
                                return void 0
                            }
                        }
                        function r(t, e) {
                            var n = i(e), r = t.params;
                            r.host = n.hostname + ":" + n.port, r.pathname = n.pathname, t.sameOrigin = n.sameOrigin
                        }
                        if (window.XMLHttpRequest && XMLHttpRequest.prototype && XMLHttpRequest.prototype.addEventListener && !/CriOS/.test(navigator.userAgent)) {
                            t("loader").features.xhr = !0;
                            var o = t("handle"), i = t(2), a = t("ee"), s = ["load", "error", "abort", "timeout"], c = s.length, f = t(1);
                            t(4), t(3), a.on("new-xhr", function () {
                                this.totalCbs = 0, this.called = 0, this.cbTime = 0, this.end = e, this.ended = !1, this.xhrGuids = {}
                            }), a.on("open-xhr-start", function (t) {
                                this.params = {method: t[0]}, r(this, t[1]), this.metrics = {}
                            }), a.on("open-xhr-end", function (t, e) {
                                "loader_config"in NREUM && "xpid"in NREUM.loader_config && this.sameOrigin && e.setRequestHeader("X-NewRelic-ID", NREUM.loader_config.xpid)
                            }), a.on("send-xhr-start", function (t, e) {
                                var r = this.metrics, o = t[0], i = this;
                                if (r && o) {
                                    var f = n(o);
                                    f && (r.txSize = f)
                                }
                                this.startTime = (new Date).getTime(), this.listener = function (t) {
                                    try {
                                        "abort" === t.type && (i.params.aborted = !0), ("load" !== t.type || i.called === i.totalCbs && (i.onloadCalled || "function" != typeof e.onload)) && i.end(e)
                                    } catch (n) {
                                        try {
                                            a.emit("internal-error", [n])
                                        } catch (r) {
                                        }
                                    }
                                };
                                for (var u = 0; c > u; u++)
                                    e.addEventListener(s[u], this.listener, !1)
                            }), a.on("xhr-cb-time", function (t, e, n) {
                                this.cbTime += t, e ? this.onloadCalled = !0 : this.called += 1, this.called !== this.totalCbs || !this.onloadCalled && "function" == typeof n.onload || this.end(n)
                            }), a.on("xhr-load-added", function (t, e) {
                                var n = "" + f(t) + !!e;
                                this.xhrGuids && !this.xhrGuids[n] && (this.xhrGuids[n] = !0, this.totalCbs += 1)
                            }), a.on("xhr-load-removed", function (t, e) {
                                var n = "" + f(t) + !!e;
                                this.xhrGuids && this.xhrGuids[n] && (delete this.xhrGuids[n], this.totalCbs -= 1)
                            }), a.on("addEventListener-end", function (t, e) {
                                e instanceof XMLHttpRequest && "load" === t[0] && a.emit("xhr-load-added", [t[1], t[2]], e)
                            }), a.on("removeEventListener-end", function (t, e) {
                                e instanceof XMLHttpRequest && "load" === t[0] && a.emit("xhr-load-removed", [t[1], t[2]], e)
                            }), a.on("fn-start", function (t, e, n) {
                                e instanceof XMLHttpRequest && ("onload" === n && (this.onload = !0), ("load" === (t[0] && t[0].type) || this.onload) && (this.xhrCbStart = (new Date).getTime()))
                            }), a.on("fn-end", function (t, e) {
                                this.xhrCbStart && a.emit("xhr-cb-time", [(new Date).getTime() - this.xhrCbStart, this.onload, e], e)
                            })
                        }
                    }, {1: "XL7HBI", 2: 12, 3: 10, 4: 6, ee: "QJf3ax", handle: "D5DuLP", loader: "G9z0Bl"}], 12: [function (t, e) {
                        e.exports = function (t) {
                            var e = document.createElement("a"), n = window.location, r = {};
                            e.href = t, r.port = e.port;
                            var o = e.href.split("://");
                            return!r.port && o[1] && (r.port = o[1].split("/")[0].split("@").pop().split(":")[1]), r.port && "0" !== r.port || (r.port = "https" === o[0] ? "443" : "80"), r.hostname = e.hostname || n.hostname, r.pathname = e.pathname, r.protocol = o[0], "/" !== r.pathname.charAt(0) && (r.pathname = "/" + r.pathname), r.sameOrigin = !e.hostname || e.hostname === document.domain && e.port === n.port && e.protocol === n.protocol, r
                        }
                    }, {}], 13: [function (t, e) {
                        function n(t) {
                            return function () {
                                r(t, [(new Date).getTime()].concat(i(arguments)))
                            }
                        }
                        var r = t("handle"), o = t(1), i = t(2);
                        "undefined" == typeof window.newrelic && (newrelic = window.NREUM);
                        var a = ["setPageViewName", "addPageAction", "setCustomAttribute", "finished", "addToTrace", "inlineHit", "noticeError"];
                        o(a, function (t, e) {
                            window.NREUM[e] = n("api-" + e)
                        }), e.exports = window.NREUM
                    }, {1: 22, 2: 23, handle: "D5DuLP"}], "7eSDFh": [function (t, e) {
                        function n(t, e, n) {
                            if (r.call(t, e))
                                return t[e];
                            var o = n();
                            if (Object.defineProperty && Object.keys)
                                try {
                                    return Object.defineProperty(t, e, {value: o, writable: !0, enumerable: !1}), o
                                } catch (i) {
                                }
                            return t[e] = o, o
                        }
                        var r = Object.prototype.hasOwnProperty;
                        e.exports = n
                    }, {}], gos: [function (t, e) {
                        e.exports = t("7eSDFh")
                    }, {}], handle: [function (t, e) {
                        e.exports = t("D5DuLP")
                    }, {}], D5DuLP: [function (t, e) {
                        function n(t, e, n) {
                            return r.listeners(t).length ? r.emit(t, e, n) : (o[t] || (o[t] = []), void o[t].push(e))
                        }
                        var r = t("ee").create(), o = {};
                        e.exports = n, n.ee = r, r.q = o
                    }, {ee: "QJf3ax"}], id: [function (t, e) {
                        e.exports = t("XL7HBI")
                    }, {}], XL7HBI: [function (t, e) {
                        function n(t) {
                            var e = typeof t;
                            return!t || "object" !== e && "function" !== e ? -1 : t === window ? 0 : i(t, o, function () {
                                return r++
                            })
                        }
                        var r = 1, o = "nr@id", i = t("gos");
                        e.exports = n
                    }, {gos: "7eSDFh"}], G9z0Bl: [function (t, e) {
                        function n() {
                            var t = p.info = NREUM.info, e = f.getElementsByTagName("script")[0];
                            if (t && t.licenseKey && t.applicationID && e) {
                                s(d, function (e, n) {
                                    e in t || (t[e] = n)
                                });
                                var n = "https" === u.split(":")[0] || t.sslForHttp;
                                p.proto = n ? "https://" : "http://", a("mark", ["onload", i()]);
                                var r = f.createElement("script");
                                r.src = p.proto + t.agent, e.parentNode.insertBefore(r, e)
                            }
                        }
                        function r() {
                            "complete" === f.readyState && o()
                        }
                        function o() {
                            a("mark", ["domContent", i()])
                        }
                        function i() {
                            return(new Date).getTime()
                        }
                        var a = t("handle"), s = t(1), c = (t(2), window), f = c.document, u = ("" + location).split("?")[0], d = {beacon: "bam.nr-data.net", errorBeacon: "bam.nr-data.net", agent: "js-agent.newrelic.com/nr-632.min.js"}, p = e.exports = {offset: i(), origin: u, features: {}};
                        f.addEventListener ? (f.addEventListener("DOMContentLoaded", o, !1), c.addEventListener("load", n, !1)) : (f.attachEvent("onreadystatechange", r), c.attachEvent("onload", n)), a("mark", ["firstbyte", i()])
                    }, {1: 22, 2: 13, handle: "D5DuLP"}], loader: [function (t, e) {
                        e.exports = t("G9z0Bl")
                    }, {}], 22: [function (t, e) {
                        function n(t, e) {
                            var n = [], o = "", i = 0;
                            for (o in t)
                                r.call(t, o) && (n[i] = e(o, t[o]), i += 1);
                            return n
                        }
                        var r = Object.prototype.hasOwnProperty;
                        e.exports = n
                    }, {}], 23: [function (t, e) {
                        function n(t, e, n) {
                            e || (e = 0), "undefined" == typeof n && (n = t ? t.length : 0);
                            for (var r = -1, o = n - e || 0, i = Array(0 > o ? 0 : o); ++r < o; )
                                i[r] = t[e + r];
                            return i
                        }
                        e.exports = n
                    }, {}], 24: [function (t, e) {
                        function n(t) {
                            return!(t && "function" == typeof t && t.apply && !t[i])
                        }
                        var r = t("ee"), o = t(1), i = "nr@wrapper", a = Object.prototype.hasOwnProperty;
                        e.exports = function (t) {
                            function e(t, e, r, a) {
                                function nrWrapper() {
                                    var n, i, s, f;
                                    try {
                                        i = this, n = o(arguments), s = r && r(n, i) || {}
                                    } catch (d) {
                                        u([d, "", [n, i, a], s])
                                    }
                                    c(e + "start", [n, i, a], s);
                                    try {
                                        return f = t.apply(i, n)
                                    } catch (p) {
                                        throw c(e + "err", [n, i, p], s), p
                                    } finally {
                                        c(e + "end", [n, i, f], s)
                                    }
                                }
                                return n(t) ? t : (e || (e = ""), nrWrapper[i] = !0, f(t, nrWrapper), nrWrapper)
                            }
                            function s(t, r, o, i) {
                                o || (o = "");
                                var a, s, c, f = "-" === o.charAt(0);
                                for (c = 0; c < r.length; c++)
                                    s = r[c], a = t[s], n(a) || (t[s] = e(a, f ? s + o : o, i, s))
                            }
                            function c(e, n, r) {
                                try {
                                    t.emit(e, n, r)
                                } catch (o) {
                                    u([o, e, n, r])
                                }
                            }
                            function f(t, e) {
                                if (Object.defineProperty && Object.keys)
                                    try {
                                        var n = Object.keys(t);
                                        return n.forEach(function (n) {
                                            Object.defineProperty(e, n, {get: function () {
                                                    return t[n]
                                                }, set: function (e) {
                                                    return t[n] = e, e
                                                }})
                                        }), e
                                    } catch (r) {
                                        u([r])
                                    }
                                for (var o in t)
                                    a.call(t, o) && (e[o] = t[o]);
                                return e
                            }
                            function u(e) {
                                try {
                                    t.emit("internal-error", e)
                                } catch (n) {
                                }
                            }
                            return t || (t = r), e.inPlace = s, e.flag = i, e
                        }
                    }, {1: 23, ee: "QJf3ax"}]}, {}, ["G9z0Bl", 4, 11, 5]);
            ;
            NREUM.info = {beacon: "bam.nr-data.net", errorBeacon: "bam.nr-data.net", licenseKey: "b3a1593558", applicationID: "6185838", sa: 1, agent: "js-agent.newrelic.com/nr-632.min.js"}
        </script>

        <link rel="shortcut icon" href="http://voiceverso.com/favicon.png" />
        <title>VoiceVerso - Transforming Bloggers into Published Authors</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo CSS; ?>bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo CSS; ?>agency.css" rel="stylesheet">


        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!--<link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
         <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>-->
        <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,400italic,600' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
           <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
         <![endif]-->
        <script src="<?php echo JS; ?>jquery.js"></script> 
        <script src="<?php echo JS; ?>modernizr.custom.js"></script>
        <script type="text/javascript" >

            $(document).ready(function () {
                $("#signupbtn").click(function (e) {
                    e.preventDefault();
                    var person_id = $("#personid").val();
                    var email = $("#singup-email").val();
                    var password = $("#singup-password").val();
                    if (email == 0 && password == 0) {
                        $("#singup-email").css("border", "1px solid red");
                        $("#singup-password").css("border", "1px solid red");
                    } else {
                        $.ajax({
                            type: "POST",
                            dataType: 'html',
                            url: "ajaxsignup.php",
                            data: {password: password, email: email}
                        }).done(function (msg) {
                            if (msg == 1) {
                                $("#already").show();
                            } else {
                                $("#confirm").show();
                                return false;
                            }
                        });

                    }
                });
                $("#loginbtn").click(function (e) {
                    e.preventDefault();
                    var email = $("#email").val();
                    var password = $("#password").val();
                    if (email == 0 && password == 0) {
                        $("#email").css("border", "1px solid red");
                        $("#password").css("border", " 1px solid red");
                    } else {
                        $.ajax({
                            type: "POST",
                            dataType: 'html',
                            url: "ajaxlogin.php",
                            data: {password: password, email: email}
                        }).done(function (msg) {
                            if (msg == 1) {
                                $("#credentials").show();
                            } else {
                                window.location = '<?php echo SITE_URL; ?>/read.php';
                                return false;
                            }
                        });
                    }

                });
                $("#forgotBtn").click(function (e) {
                    e.preventDefault();
                    var email = $("#forgot-email").val();
                    var validEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (validEmail.test(email) == false) {
                        $("#forgot-email").css("border", "1px solid red");
                    } else {
                        $.ajax({
                            type: "POST",
                            dataType: 'html',
                            url: "forgot.php",
                            data: {email: email}
                        }).done(function (msg) {
                            if (msg != 1) {
                                //$("#mailF").css("display","block");
                                $("#modal-3.md-modal.md-effect-12.md-show").removeClass("md-show");
                                $("#modal_forgot_pass_status.md-modal.md-effect-12").addClass("md-show");
                                $('#success').css("display", "block");
                            } else {
                                //$("#mailE").css("display","block");	              
                                $("#modal-3.md-modal.md-effect-12.md-show").removeClass("md-show");
                                $("#modal_forgot_pass_status.md-modal.md-effect-12").addClass("md-show");
                                $('#fail').css("display", "block");
                                $("#fail form .form-group .col-sm-5 input").css("border", "1px solid red");
                                return false;
                            }
                        })
                    }
                });

                $("#forgotBtnFail").click(function (e) {
                    e.preventDefault();
                    var email = $("#forgot-email-fail").val();
                    var validEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (validEmail.test(email) == false) {
                        $("#forgot-email-fail").css("border", "1px solid red");
                    } else {
                        $.ajax({
                            type: "POST",
                            dataType: 'html',
                            url: "forgot.php",
                            data: {email: email}
                        }).done(function (msg) {
                            if (msg != 1) {
                                //$("#mailF").css("display","block");
                                //$("#modal-3.md-modal.md-effect-12.md-show").removeClass("md-show");
                                //$("#modal_forgot_pass_status.md-modal.md-effect-12").addClass("md-show");
                                $('#fail').css("display", "none");
                                $('#success').css("display", "block");
                            } else {
                                //$("#mailE").css("display","block");	              
                                //$("#modal-3.md-modal.md-effect-12.md-show").removeClass("md-show");
                                //$("#modal_forgot_pass_status.md-modal.md-effect-12").addClass("md-show"); 
                                $('#success').css("display", "none");
                                $('#fail').css("display", "block");
                                $("#fail form .form-group .col-sm-5 input").css("border", "1px solid red");
                                return false;
                            }
                        })
                    }
                });

                $(".forgot").click(function () {
                    $("#modal-12.md-modal.md-effect-12.md-show").removeClass("md-show");
                });

                $("#signup").click(function () {
                    $("#modal_forgot_pass_status.md-modal.md-effect-12.md-show").removeClass("md-show");
                });

                $(".sign").click(function () {
                    $("#modal-3.md-modal.md-effect-12.md-show").removeClass("md-show");
                });


                $('.close').click(function () {
                    $(this).parent().parent().removeClass("md-show");
                });


            });
        </script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-44727401-2', 'auto');
            ga('send', 'pageview');

        </script>
        <script type="text/javascript">
            $(".social_read").on("click", function (e) {
                e.preventDefault();
                $("body, html").animate({scrollTop: $($(this).attr('href')).offset().top}, 800);
            });
        </script>

        <script type="text/javascript" language="javascript">
            function clickme(postid) {

                popUp = window.open('https://plus.google.com/share?url=http://voiceverso.com/voice.php?postid=' + postid, 'popupwindow', 'scrollbars=yes,width=800,height=400');
                popUp.focus();
                return false;

            }
        </script>
        <!-- start Mixpanel --><script type="text/javascript">(function (f, b) {
                if (!b.__SV) {
                    var a, e, i, g;
                    window.mixpanel = b;
                    b._i = [];
                    b.init = function (a, e, d) {
                        function f(b, h) {
                            var a = h.split(".");
                            2 == a.length && (b = b[a[0]], h = a[1]);
                            b[h] = function () {
                                b.push([h].concat(Array.prototype.slice.call(arguments, 0)))
                            }
                        }
                        var c = b;
                        "undefined" !== typeof d ? c = b[d] = [] : d = "mixpanel";
                        c.people = c.people || [];
                        c.toString = function (b) {
                            var a = "mixpanel";
                            "mixpanel" !== d && (a += "." + d);
                            b || (a += " (stub)");
                            return a
                        };
                        c.people.toString = function () {
                            return c.toString(1) + ".people (stub)"
                        };
                        i = "disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
                        for (g = 0; g < i.length; g++)
                            f(c, i[g]);
                        b._i.push([a, e, d])
                    };
                    b.__SV = 1.2;
                    a = f.createElement("script");
                    a.type = "text/javascript";
                    a.async = !0;
                    a.src = "undefined" !== typeof MIXPANEL_CUSTOM_LIB_URL ? MIXPANEL_CUSTOM_LIB_URL : "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";
                    e = f.getElementsByTagName("script")[0];
                    e.parentNode.insertBefore(a, e)
                }
            })(document, window.mixpanel || []);
            mixpanel.init("496625c1762126276a4a8d22923acd48");</script><!-- end Mixpanel -->

        <script>

            function show_my_ico(id) {

                var re = new RegExp("_");

                if (id != '' && re) {

                    var total;

                    total = id.split("_");

                    var menuId = total[1];

                    var targetId = 'social_read_' + menuId;

                    $('#' + targetId).toggleClass("social_read unhidden");



                }

                return false;

            }


        </script> 
        <script type="text/javascript">
            var wizrocket = {event: [], profile: [], account: [], enum: function (e) {
                    return '$E_' + e
                }};
            wizrocket.account.push({"id": "466-ZRZ-7K4Z"});
            (function () {
                var wzrk = document.createElement('script');
                wzrk.type = 'text/javascript';
                wzrk.async = true;
                wzrk.src = ('https:' == document.location.protocol ? 'https://d2r1yp2w7bby2u.cloudfront.net' : 'http://static.wizrocket.com') + '/js/a.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(wzrk, s);
            })();
        </script>


    </head>


    <body id="page-top" class="index myindex">
        <!-- facebook share -->
        <!--<div id="fb-root"></div>
        <script type="text/javascript">
         function statusChangeCallback(response) {
            console.log('statusChangeCallback');
            console.log(response);
            // The response object is returned with a status field that lets the
            // app know the current login status of the person.
            // Full docs on the response object can be found in the documentation
            // for FB.getLoginStatus().
            if (response.status === 'connected') {
              // Logged into your app and Facebook.
              testAPI();
            } else if (response.status === 'not_authorized') {
              // The person is logged into Facebook, but not your app.
              document.getElementById('status').innerHTML = 'Please log ' +
                'into this app.';
            } else {
              // The person is not logged into Facebook, so we're not sure if
              // they are logged into this app or not.
              document.getElementById('status').innerHTML = 'Please log ' +
                'into Facebook.';
            }
          }
        
          // This function is called when someone finishes with the Login
          // Button.  See the onlogin handler attached to it in the sample
          // code below.
          function checkLoginState() {
            FB.getLoginStatus(function(response) {
              if (response.status === 'connected') {
            console.log(response.authResponse.accessToken);
              statusChangeCallback(response);
             }
            });
        
          
            window.fbAsyncInit = function() {
                        
                FB.init({
                        appId      : '1593320104239178', 
                        channelUrl : 'http://voiceverso.com/', 
                        status     : true, 
                        cookie     : true,
                        xfbml      : true,
                        version    : 'v2.2',
                        oauth : true
                });
        
        
            // Now that we've initialized the JavaScript SDK, we call 
          // FB.getLoginStatus().  This function gets the state of the
          // person visiting this page and can return one of three states to
          // the callback you provide.  They can be:
          //
          // 1. Logged into your app ('connected')
          // 2. Logged into Facebook, but not your app ('not_authorized')
          // 3. Not logged into Facebook and can't tell if they are logged into
          //    your app or not.
          //
          // These three cases are handled in the callback function.
        
          FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
          });
        
          }
        
          // Load the SDK asynchronously
          (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
        }
        
          // Here we run a very simple test of the Graph API after login is
          // successful.  See statusChangeCallback() for when this call is made.
         
        </script>-->

        <!-- Twitter share -->
        <script type="text/javascript" async src="//platform.twitter.com/widgets.js"></script>
        <!-- G+ share-->
        <script src="https://apis.google.com/js/platform.js" async defer></script>


        <script type="text/javascript">
            mixpanel.track("Page Loaded", {"Page Name": "Landing Page"});
        </script>
        <!-- Navigation Default -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container"> 
                <!--social icons at the left --->


                <a class="navbar-brand page-scroll" href="#page-top">
                    <img alt="voice verso" src="images/new-v.fw.png"></a>


                <ul class="nav navbar-nav navbar-right" id="shoutlogin">


                    <li class="sing ink-it"><a style="display:none;" class="btn shout-btn btn-primary fl shoutbutton md-trigger" data-modal="modal-12" title="Start Writing!">Start Writing</a></li>
                   <!--<li class="sing"><a class="page-scroll" data-toggle="tooltip" data-placement="bottom" title="VoiceVerso-Blog"href="http://blog.voiceverso.com" target="_new"><i class="fa fa-pencil-square-o"></i></a></li>
                  <li class="sing"> <a class="page-scroll" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="https://www.facebook.com/pages/Voice-Verso/1540202696240165"><i class="fa fa-facebook"></i></a> </li>
                  <li class="sing"> <a class="page-scroll" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="https://twitter.com/VoiceVerso"><i class="fa fa-twitter"></i></a> </li>
                  <li class="sing"><a href="https://plus.google.com/103960191639723691174" rel="publisher" class="page-scroll" data-toggle="tooltip" data-placement="bottom" title="Google+"><i class="fa fa-google-plus"></i></a> </li>
                 <li class="sing"> <a class="page-scroll" data-toggle="tooltip" data-placement="bottom" title="LinkedIn" href="https://www.linkedin.com/company/voice-verso"><i class="fa fa-linkedin"></i></a> </li>-->
                    <!--<li class = "sing"><a class="btn btn-xl shoutbutton" title="Start Writing" data-target="#loginModal" data-toggle="modal" data-popover="Shout your story loud!" data-popover-type="sign-in" data-action="sign-in-prompt" href="#">Start Writing</a></li>-->

                </ul>
            </div>
            <!-- /.container-fluid --> 
        </nav>
        <!-- Header -->
        <header>
            <div class="container">

                <div class="intro-text">
                    <!--<div class="intro-lead-in"></div>-->
                    <img src="images/VV_logo-new.jpg" alt="Voice Verso" id="logo" width="250px" height="179px">



                    <div class="col-md-12 col-sm-12 col-xs-12" id="intro-heading">
                        <h1 class="test">Transforming Blogger into published authors</h1>


                        </span>
                    </div>
                    <!--<div class="col-sm-12 colxs-12" id="intro-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                     sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis 
                      nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
           
                     </div>-->
                    <div class="loginbtn">
                        <a title="Start Writing" class="btn btn-xl shoutbutton md-trigger" data-modal="modal-12">Start Writing</a>
                    </div>


                </div>


            </div>
        </header>

        <!-- Services Section -->

        <section id="services">
            <div class="container" id="land-stories">
                <div class="postread" id ="postList">
                    <h2 class="section-heading">See what our awesome Writers are Voicing out?</h2>





                    <?php
                    $j = 1;

                    while ($row = mysql_fetch_array($result)) {



                        $string1 = strip_tags($row['title']);

                        if (strlen($string1) > 30) {

                            $stringCut = substr($string1, 0, 30);



                            // the title char counter limitation

                            $string1 = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
                        }



                        $string = strip_tags($row['content']);



                        if (strlen($string) > 350) {



                            // truncate string

                            $stringCut = substr($string, 0, 350);



                            // the content with 'read more' functionality

                            $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '... <a class="read-more" href=voice.php?postid=' . $row["post_id"] . '>Read More</a>';
                        }

                        $date = date('M-d-Y', strtotime(($row['datetime'])));
                        //$date = $row['datetime'];

                        echo '<section class="postBlock post_view">
		            <div class="row">
                <div class="container">
                <div class ="col-md-10 col-sm-12 col-xs-11">';

                        if ($row['profile'] == "") {



                            echo $image = '<div class="col-md-2"></div><div class="col-md-10 col-xs-12"><img src="' . IMAGES . 'default_profile_pic-transprint-2.png" width = "32" height ="32" title = "Profile Pic!" class = "thum-img img-circle" alt ="' . $row["pename"] . '"/>';

                            echo '
	

			 
			  

			  <a href = "mystories.php?id=' . $row["person_id"] . '" class ="pename" id ="pen">' . $row["pename"] . '</a>

					 <p class ="bio" id ="bio">' . $row["bio"] . '</p></div>

				';
                        } else {

                            echo $image = '<div class="col-md-2"></div><div class="col-md-10 col-xs-12"><img class= "img-circle thum-img" title ="' . $row["pename"] . '" src="' . UPLOAD . $row["profile"] . '" width = "32" height ="32" class = "thumbnail" alt ="' . $row["pename"] . '"/>';

                            echo '
			
			<a href = "mystories.php?id=' . $row["person_id"] . '" class ="pename">' . $row["pename"] . '</a>

					 <p class ="bio">' . $row["bio"] . '</p>

					 

				</div>';
                        };


                        $_SESSION['user_id'] = $row["person_id"];
                        echo "<div class='col-xs-2'></div><div class='col-md-10 col-xs-12'><a class = 'title' maxlength = '50' href = 'voice.php?postid=" . $row['post_id'] . "'>" . $string1 . "</a>
                                       
			    <div class='story-page'>

			     <p>" . $string . "</p>

           
			  
			   </div>

			   </div>
            </div>
            </div>

            </div> 
			   </section>";

                        $j++;
                    }
                    ?>

                </div>




        </section>
        <!-- Landing page footer -->
        <footer>
            <div class="container">
                <!--social icons -->


                <div class="alignment">

                    <!-- <ul class="list-inline quicklinks">
                               <li><a class="blog" href="http://blog.voiceverso.com" target="_new">Blog</a></li>
                               <li><a class="blog" href="privacy.php">Privacy Policy</a></li>
                               <li><a  class="blog" href="terms.php">Terms of Use</a></li>
                           </ul>-->



                </div>
                <div class="alignment"><i class="fa fa-copyright"></i> VoiceVerso 2015-2016</div>
                <div class="alignment"> 
                    <ul class="list-inline social-buttons">
                        <li><a href="https://www.facebook.com/pages/Voice-Verso/1540202696240165" title="Facebook"><i class="fa fa-facebook"></i></a> </li>
                        <li><a href="https://twitter.com/VoiceVerso"><i class="fa fa-twitter" title="Twitter"></i></a> </li>
                        <li><a href="https://plus.google.com/103960191639723691174" title="Google+"><i class="fa fa-google-plus"></i></a> </li>
                        <!--<li><a href="https://www.linkedin.com/company/voice-verso"><i class="fa fa-linkedin"></i></a> </li>-->
                        <li><a href="https://www.linkedin.com/company/voice-verso" title="Instagram"><i class="fa fa-instagram"></i></a> </li>
                    </ul>
                </div>




            </div>
        </footer>
        <!-- Portfolio Modals --> 
        <!-- Use the modals below to showcase details about your portfolio projects! --> 
        <!-- Modal  after clicking shout now button-->
        <div class="md-modal md-effect-12" id="modal-12">
            <div class="modal-header"><button class="md-close close">X</button></div>
            <div class="md-content">
                <div class="modal-body">

                    <p class="overlay-title">LOGIN</p>

                    <div>

                        <!--soial login-->
                        <ul style="list-style:none;">
                            <!--             <li><fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button></li>
                                         <div id="status"></div>-->
                            <a href="javascript:void(0)" onclick="Login()"><li><button class="button-fb">Connect Via <i class="fa fa-facebook-square"></i></button></li></a>
                            <!--<a href="process.php"> <li><button class="button-tw">Connect Via <i class="fa fa-twitter"></i></button></li></a>
                                        <li><button class="button-g">Connect Via <i class="fa fa-google-plus"></i></button></li>-->
                        </ul>

                        <div class= "form1">

                            <form class="form-horizontal" role="form" method ="POST"  id ="loginForm">

                                <!-- Login form in the modal-->

                                <span style="color:red"></span>


                                <div id = "credentials" style = "display: none; color: red; text-align:left; margin-left:38%; font-size:14px;">
                                    Invalid Credentials, Try Again!
                                </div>

                                <div class="form-group">

                                    <label class="col-sm-4 control-label"><span class="fa fa-envelope" title ="Email"></span></label>

                                    <div class="col-sm-5">

                                        <input id="email" type="text" name ="email" title="Email goes here!" placeholder="Email Address" class="form-control" required/>

                                    </div>

                                </div>

                                <div class="form-group"> 

                                    <!--Password Login-->

                                    <label class="col-sm-4 control-label"><span class="fa fa-lock" title ="Password"></span></label>

                                    <div class="col-sm-5">

                                        <input id="password" type="password" title="Password goes here!" placeholder="Password" name = "password" class="form-control" maxlength ="15" required/>
                                    </div>


                                </div>
                                <ul style="list-style:none">                          
                                    <li><input type="submit" class= "btn btn-primary btn-lg btn_login" id ="loginbtn" value="Login" name="login" title ="Log in with Voice Verso."/></li>








                                    <li><a href ="javascript:void(0);" na class="forgot link md-trigger md-close" data-modal="modal-3">Forgot Password?</a></li>
                                    <?php /* ?><li><a type ="link" href ="" class = "link" data-toggle="modal" data-target="#iModal" id = "forgot">Forgot Password?</a></li><?php */ ?>
                                </ul>



                                <!----------------------------------------------------------------------------------------->


                                <?php
//                   include_once("fb_config.php");
//include_once("includes/fb_functions.php");
//	$fbloginUrl = $facebook->getLoginUrl(array('redirect_uri'=>$homeurl,'scope'=>$fbPermissions));
//
//                        if(!$fbuser){
//                        $fbuser = null;
//                        }else{
//        $user_profile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
//        $user = new Users();
//        
//	$user_data = $user->checkUser('facebook',$user_profile['id'],$user_profile['first_name'],$user_profile['last_name'],$user_profile['email'],$user_profile['picture']['data']['url']);
//       
//                    if($user_data){
//   header('Location:read.php');
//}
//        
//        
//                        }
                                ?> 
                                <script>
                                    window.fbAsyncInit = function () {
                                        FB.init({
                                            appId: '1767973176767525', // Set YOUR APP ID

                                            status: true, // check login status
                                            cookie: true, // enable cookies to allow the server to access the session
                                            xfbml: true  // parse XFBML
                                        });

                                        FB.Event.subscribe('auth.authResponseChange', function (response)
                                        {
                                            if (response.status === 'connected')
                                            {
                                                //        document.getElementById("message").innerHTML +=  "<br>Connected to Facebook";
                                                //SUCCESS

                                            }
                                            else if (response.status === 'not_authorized')
                                            {
                                                //        document.getElementById("message").innerHTML +=  "<br>Failed to Connect";

                                                //FAILED
                                            } else
                                            {
                                                //        document.getElementById("message").innerHTML +=  "<br>Logged Out";

                                                //UNKNOWN ERROR
                                            }
                                        });

                                    };

                                    function Login()
                                    {

                                        FB.login(function (response) {
                                            if (response.authResponse)
                                            {
                                                getUserInfo('login');
                                            } else
                                            {
                                                console.log('User cancelled login or did not fully authorize.');
                                            }
                                        }, {scope: 'email,user_photos,user_videos'});

                                    }

                                    function signUp()
                                    {

                                        FB.login(function (response) {
                                            if (response.authResponse)
                                            {
                                                getUserInfo('signup');
                                            } else
                                            {
                                                console.log('User cancelled login or did not fully authorize.');
                                            }
                                        }, {scope: 'email,user_photos,user_videos'});

                                    }



                                    function getUserInfo(param) {
                                        FB.api('/me?fields=id,name,link,first_name,last_name,email,gender,locale,picture', function (response) {

                                            console.log(response);
                                            if (param == "login") {

                                                $.post('includes/fb_functions.php', response, function (returnedData) {
                                                    // do something here with the returnedData
                                                    console.log(returnedData);
                                                    if (returnedData != "") {
                                                        window.location.href = 'read.php';
                                                    }
                                                });
                                            } else {
                                                $.post('includes/fb_functions_signup.php', response, function (returnedData) {
                                                    // do something here with the returnedData
                                                    console.log(returnedData);
                                                    if (returnedData == 1) {
                                                          $("#already").show();
                                                    } else {
                                                        window.location.href = 'home.php?id='+returnedData;
                                                    }
                                                });
                                            }


                                            //      var str="<b>Name</b> : "+response.name+"<br>";
                                            //      var str="<b>Name</b> : "+response.first_name+"<br>";
                                            //          str +="<b>Link: </b>"+response.link+"<br>";
                                            //          str +="<b>Username:</b> "+response.username+"<br>";
                                            //          str +="<b>id: </b>"+response.id+"<br>";
                                            //          str +="<b>Email:</b> "+response.email+"<br>";
                                            //          str +="<input type='button' value='Get Photo' onclick='getPhoto();'/>";
                                            //          str +="<input type='button' value='Logout' onclick='Logout();'/>";
                                            //          document.getElementById("status").innerHTML=str;

                                        });
                                    }
                                    function getPhoto()
                                    {
                                        FB.api('/me/picture?type=normal', function (response) {

                                            var str = "<br/><b>Pic</b> : <img src='" + response.data.url + "'/>";
                                            //          document.getElementById("status").innerHTML+=str;

                                        });
                                    }
                                    function Logout()
                                    {
                                        FB.logout(function () {
                                            document.location.reload();
                                        });
                                    }

                                    // Load the SDK asynchronously
                                    (function (d) {
                                        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                                        if (d.getElementById(id)) {
                                            return;
                                        }
                                        js = d.createElement('script');
                                        js.id = id;
                                        js.async = true;
                                        js.src = "//connect.facebook.net/en_US/all.js";
                                        ref.parentNode.insertBefore(js, ref);
                                    }(document));</script>


       <!--<a href="<?php echo $fbloginUrl ?>"><img src="images/fb_login.png"></a>-->

                                <!--                         // twitter-->


<!--<a href="process.php"><img src="images/sign-in-with-twitter.png" width="151" height="24" border="0" /></a>-->













                                <!----------------------------------------------------------------------------------------->

                            </form>


                        </div>
                        Your First Time? -<a class="link md-trigger md-close2" id="sign" data-modal="modal-1">SignUp</a>
                    </div>
                </div>
            </div>
        </div>






    </div>
</div><!-- /container -->


<div class="md-modal md-effect-12" id="modal-1">
    <div class="modal-header"><button class="md-close close">X</button></div>
    <div class="md-content">
        <div class="modal-body">

            <p class="overlay-title">SIGNUP</p>

            <div>


                <ul style="list-style:none;">
                    <a href="javascript:void(0)" onclick="signUp()"><li><button class="button-fb">Connect Via <i class="fa fa-facebook-square"></i></button></li></a>
                  <!--<li><button class="button-tw">Connect Via <i class="fa fa-twitter"></i></button></li>
                  <li><button class="button-g">Connect Via <i class="fa fa-google-plus"></i></button></li>-->
                </ul>


                <div class ="form2">

                    <form class="form-horizontal" role="form" method ="POST" id="userForm">

                        <!-- Sign Up Form--> 


                        <div id = "confirm" style = "display: none; color: green;">

                            Thank you for Signing In. An Email has been sent to your registered Email Address. Please confirm and proceed!

                        </div>
                        <div id = "already" style = "display: none; color: red; font-size:14px;">

                            Email already exists. Try again.

                        </div>

                        <div class="form-group"> 

                            <!-- Sign Up Email--> 

                            <label class="col-sm-4 control-label"><span class="fa fa-envelope" title ="Email"></span></label>

                            <div class="col-sm-5">

                                <input type="text" name ="email" title="Email goes here!" placeholder="Email Address"  id="singup-email" class="form-control" required/>

                                <input type="hidden" id ="personid" value="<?php echo $user_id; ?>">

                            </div>

                        </div>

                        <!-- Sign Up Password--> 

                        <div class="form-group">

                            <label class="col-sm-4 control-label"><span class="fa fa-lock" title ="Password"></span></label>

                            <div class="col-sm-5">

                                <input type="password" name= "password" title="Password goes here!" placeholder="Password"  id ="singup-password" class="form-control" maxlength="15" required/>

                            </div>

                        </div>



                        <input type="submit" name="signup" class= "btn btn-success btn-lg btn_login" id ="signupbtn" value="SignUp" title="Sign up with Voice Verso"/>

                    </form>



















                </div>
                Already a User? -<a class="link md-trigger md-close2" id="logn" data-modal="modal-12">LogIn</a>
            </div>

        </div>
    </div>
</div>
</div>
<div class="md-modal md-effect-12" id="modal-2">
    <div class="modal-header"><button class="md-close close">X</button></div>
    <div class="md-content">
        <div class="modal-body">

            <p class="overlay-title">SIGNUP</p>

            <div>


                <ul style="list-style:none;">
                 <!--<li><button class="button-fb">Connect Via <i class="fa fa-facebook-square"></i></button></li>-->
                  <!--<li><button class="button-tw">Connect Via <i class="fa fa-twitter"></i></button></li>
                  <li><button class="button-g">Connect Via <i class="fa fa-google-plus"></i></button></li>-->
                </ul>


                <div class ="form2">

                    <form class="form-horizontal" role="form" method ="POST" id="userForm">

                        <!-- Sign Up Form--> 


                        <div id = "confirm" style = "display: none; color: green;">

                            Thank you for Signing In. An Email has been sent to your registered Email Address. Please confirm and proceed!

                        </div>
                        <div id = "already" style = "display: none; color: red; font-size:14px;">

                            Email already exists. Try again.

                        </div>

                        <div class="form-group"> 

                            <!-- Sign Up Email--> 

                            <label class="col-sm-4 control-label"><span class="fa fa-envelope" title ="Email"></span></label>

                            <div class="col-sm-5">

                                <input type="text" name ="email" title="Email goes here!" placeholder="Email Address"  id="singup-email" class="form-control" required/>

                                <input type="hidden" id ="personid" value="<?php echo $user_id; ?>">

                            </div>

                        </div>

                        <!-- Sign Up Password--> 

                        <div class="form-group">

                            <label class="col-sm-4 control-label"><span class="fa fa-lock" title ="Password"></span></label>

                            <div class="col-sm-5">

                                <input type="password" name= "password" title="Password goes here!" placeholder="Password"  id ="singup-password" class="form-control" maxlength="15" required/>

                            </div>

                        </div>



                        <input type="submit" name="signup" class= "btn btn-success btn-lg btn_login" id ="signupbtn" value="SignUp" title="Sign up with Voice Verso"/>

                    </form>

                </div>
                Already a User? -<a class="link md-trigger md-close2" id="logn" data-modal="modal-12">LogIn</a>
            </div>

        </div>
    </div>
</div>
</div>

<div class="md-modal md-effect-12" id="modal-3">
    <div class="modal-header"><button class="md-close close">X</button></div>
    <div class="md-content">
        <div class="modal-body">

            <p class="overlay-title">EMAIL</p>

            <div class="form-3"> 
                <form class="form-horizontal" role="form" method ="POST" id="userForm">
                    <div id = "mailF" style = "display: none; color: #000;">

                        A Mail has been sent to your registered email Address. Follow Along.

                    </div>
                    <div id = "mailF" style = "display: none; color: green;">

                        Sorry, the email you have entered is not available. 
                    </div>   
                    <div class="form-group"> 

                        <label class="col-sm-4 control-label"><span class="fa fa-lock" title ="Password"></span></label>

                        <div class="col-sm-5">

                            <input type="email" name= "email" title="Email goes here!" placeholder="Your Email Address"  id ="forgot-email" class="form-control" required/>

                        </div>
                        <input type="submit" name="forgot" class= "btn btn-success btn-lg btn_login" id ="forgotBtn" value="Done" title="Generate new Password"/>
                    </div>

                </form>  
                Your First Time? -<a class="sign link md-trigger md-close2" id="sign" data-modal="modal-1">SignUp</a>
            </div>
        </div>
    </div>
</div> 

<!---Forgot Password Success Pop Up Starts-->
<div class="md-modal md-effect-12" id="modal_forgot_pass_status">
    <div class="modal-header">
        <button class="md-close close">X</button>
    </div>
    <div class="md-content">
        <div class="modal-body">         
            <?php /* ?><p class="overlay-title">EMAIL</p>              <?php */ ?>
            <div class="form-3">       
                <div id = "success" style = "display: none; ">
                    <h2>A Mail has been sent to your registered email Address. Follow Along.</h2>
                </div>
                <div id = "fail" style = "display: none;">								 				 
                    <p class="overlay-title">EMAIL</p> 
                    <form class="form-horizontal" role="form" method ="POST" id="userForm">         
                        <div class="form-group"> 
                            <label class="col-sm-4 control-label"><span class="fa fa-lock" title ="Password"></span></label>
                            <div class="col-sm-5">
                                <input type="email" name= "email" title="Invalid email address" placeholder="Your Email Address"  id ="forgot-email-fail" class="form-control" required/>
                            </div>
                            <input type="submit" name="forgot" class= "btn btn-success btn-lg btn_login" id ="forgotBtnFail" value="Done" title="Generate new Password"/>
                        </div>                 
                    </form>  
                    Your First Time? -<a class="sign link md-trigger md-close2" id="signup" data-modal="modal-1">SignUp</a>
                </div>   
            </div>
        </div>
    </div>
</div>     
<!---Forgot Password Success Pop Up Ends-->
<div class="md-overlay"></div>
<!-- jQuery --> 
<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.min.js"></script> 
<!-- Plugin JavaScript --> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 
<script src="js/classie.js"></script> 
<script src="js/cbpAnimatedHeader.js"></script> 
<script type="text/javascript">
                        $(function () {
                            $('[data-toggle="tooltip"]').tooltip()
                        })
</script> 
<script src="js/modalEffects.js"></script><!-- Custom Theme JavaScript -->
<script>
                        // this is important for IEs
                        var polyfilter_scriptpath = '/js/';</script>
<script src="js/cssParser.js"></script>
<script src="js/css-filters-polyfill.js"></script>
<!-- Custom Theme JavaScript -->
</body>
</html>
