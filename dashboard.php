<?php 
   include_once('includes/connect.php');
   include('header.php');
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<title>Dashboard - VoiceVerso</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.CarouSlide.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".s1").CarouSlide({
			animType:"slide-vertical",
			autoAnim:false,
			slideTime:5000,
			animTime:800,
			alwaysNext:true
		});
		$(".s2").CarouSlide({
			slideTime:8000,
			animTime:1200
		});
		$(".s3").CarouSlide({
			animTime:800,
			animType:"slide",
			animInfinity:true,
			showSlideNav:false,
			autoAnim:true
		});
	});
</script>
<link rel="stylesheet" type="text/css" href="css/CarouSlide.css" />
<link rel="stylesheet" type="text/css" href="css/storyshout.css" />
<style type="text/css">
	/*customisation styles*/
	/*body {font-family:Arial,sans-serif; font-size:0.8em;} */
	#a1, #b1, .b1, #c1 {background-color:#ffc;}
	#a2, #b2, .b2, #c2 {background-color:#ccc;}
	#a3, #b3, .b3, #c3 {background-color:#017163; color:#fff;}
	#a4, #b4, .b4, #c4 {background-color:#f0c;}
	#a5, #b5, .b5, #c5 {background-color:#aaf;}
	#a6, #b6, .b6, #c6 {background-color:#000; color:#fff;}
	#a7, #b7, .b7, #c7 {background-color:#037eb0; color:#fff;}
	#a8, #b8, .b8, #c8 {background-color:#B6D862;}
	.slider-nav .active {font-weight:bold;}
	
	.s1 {position:absolute; max-width:500px; top:14%; width:80%;}
	.s1 .slider-wrapper {border: 1px solid #ccc; position:relative;}
	.s1 .slider-wrapper {width:80%; height:295px;float: left;}
	.s1 .slider-holder {width:600px; display:block; height:295px;}
	.s1 .slider-holder li.slide {width:400px; height:275px; padding-right:210px;}
	.s1 .slider-nav {right:-1px; top:0; z-index:1000; width:20%; float: left; padding:0; margin:0; border:1px solid #ccc; border-width:1px 0 0 1px; background-color:#fff;}
	.s1 .slider-nav li {list-style:none; height:36px; margin:0; padding:0;border:1px solid #ccc; border-width:0 0 1px;}
	.s1 .slider-nav li a {display:block; width:180px; padding:10px 10px; }
	.s1 .slider-nav li a:hover,
	.s1 .slider-nav li.active a {background-color:#ddd;}
	
	.s2 {position:relative; height:200px;}
	.s2 .slider-wrapper {border: 1px solid #ccc; position:relative; height:200px;}
	.s2 .slider-holder {display:block; height:200px;}
	.s2 .slider-holder li.slide {height:190px; padding:5px; width:390px; background-color:#fff;}
	.s2 .slider-nav {position:absolute; bottom:5px; z-index:100; margin-bottom:0; }
	.s2 .slider-nav li {list-style:none; float:left;}
	.s2 .slider-nav a:link,
	.s2 .slider-nav a:visited {border:none; text-indent:-9999px; overflow:hidden; width:40px; height:40px; display:block; margin:3px 11px 3px 3px;}
	.s2 .slider-nav .active a:link,
	.s2 .slider-nav .active a:visited,
	.s2 .slider-nav a:hover,
	.s2 .slider-nav a:focus,
	.s2 .slider-nav a:active {margin:0 8px 0 0; border:3px solid #00f;}
	
	.s3 {position:relative; height:200px;}
	.s3 .slider-wrapper {border: 1px solid #ccc; position:relative; height:200px; width:800px;}
	.s3 .slider-holder {display:block; height:200px; width:800px;}
	.s3 .slider-holder li.slide {height:190px; padding:5px; width:390px; background-color:#fff;}
	.s3 .slider-nav {position:absolute; bottom:5px; z-index:100; margin-bottom:0; }
	.s3 .slider-nav li {list-style:none; float:left;}
	.s3 .slider-nav a:link,
	.s3 .slider-nav a:visited {border:none; text-indent:-9999px; overflow:hidden; width:40px; height:40px; display:block; margin:3px 11px 3px 3px;}
	.s3 .slider-nav .active a:link,
	.s3 .slider-nav .active a:visited,
	.s3 .slider-nav a:hover,
	.s3 .slider-nav a:focus,
	.s3 .slider-nav a:active {margin:0 8px 0 0; border:3px solid #00f;}
	
</style>
</head>

<body>


<div class="CarouSlide s1">
	<ul class="slider-nav">
		<li><a href="#a1">item 1</a></li>
		<li><a href="#a2">item 2</a></li>
		<li><a href="#a3">item 3</a></li>
		<li><a href="#a4">item 4</a></li>
		<li><a href="#a5">item 5</a></li>
		<li><a href="#a6">item 6</a></li>
		<li><a href="#a7">item 7</a></li>
		<li><a href="#a8">item 8</a></li>
	</ul>
	<ul class="slider-holder">
		<li id="a1">
			<h2>1</h2>
			<ul>
				<li>adsff</li>
				<li>adsff</li>
			</ul>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pretium lectus et est. In hac habitasse platea dictumst.</p>
		</li>
		<li id="a2">
			<h2>2</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pretium lectus et est. In hac habitasse platea dictumst. Vestibulum cursus, purus in iaculis euismod, nisi urna gravida ligula, sit amet pulvinar ipsum ipsum ac odio. Curabitur a turpis vel dolor porttitor varius. Nullam ut purus.</p>
		</li>
		<li id="a3">
			<h2>3</h2>
			<p>Dui diam rhoncus arcu, at cursus sem lectus sit amet sem. Vestibulum iaculis, nisi in hendrerit vulputate, tortor ligula gravida lectus, non fermentum risus dolor vel dui. Aenean sed ligula nec libero facilisis consequat. Curabitur sit amet libero. In non nunc vel ligula scelerisque tincidunt. Donec id lectus. Nunc sodales. Fusce leo pede, congue vel, feugiat sed, porta porttitor, nisl.</p>
		</li>
		<li id="a4">
			<h2>4</h2>
			<p>Etiam mauris lectus, condimentum ac, consectetur in, euismod tincidunt, magna. Fusce massa. Nullam ultricies, dui a commodo vulputate, est orci porttitor nulla, sit amet dapibus lacus nulla eu erat. Sed imperdiet, lacus sit amet hendrerit posuere, dui diam rhoncus arcu, at cursus sem lectus sit amet sem. Vestibulum iaculis, nisi in hendrerit vulputate, tortor ligula gravida lectus, non fermentum risus dolor vel dui. Aenean sed ligula nec libero facilisis consequat. Curabitur sit amet libero. In non nunc vel ligula scelerisque tincidunt. Donec id lectus. Nunc sodales. Fusce leo pede, congue vel, feugiat sed, porta porttitor, nisl.</p>
		</li>
		<li id="a5">
			<h2>5</h2>
			<p>Nunc sodales. Fusce leo pede, congue vel, feugiat sed, porta porttitor, nisl.</p>
		</li>
		<li id="a6">
			<h2>6</h2>
			<p>Nunc sodales. Fusce leo pede, congue vel, feugiat sed, porta porttitor, nisl.</p>
		</li>
		<li id="a7">
			<h2>7</h2>
			<p>Nunc sodales. Fusce leo pede, congue vel, feugiat sed, porta porttitor, nisl.</p>
		</li>
		<li id="a8">
			<h2>8</h2>
			<p>Nunc sodales. Fusce leo pede, congue vel, feugiat sed, porta porttitor, nisl.</p>
		</li>
	</ul>
	
</div>

</body>
</html>